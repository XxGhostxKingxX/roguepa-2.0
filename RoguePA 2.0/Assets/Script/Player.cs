﻿using UnityEngine;

public class Player : MonoBehaviour {

    private float inputDirection;                      // X value of MoveVector
    private float verticalVelocity;                    // Y value of MoveVector

    private float speed = 5.0f;
    private float gravity = 23.0f;
    private float jumpForce = 10.0f;
    private bool secondJumpAvail = false;

    private Vector3 moveVector;
    private Vector3 lastMotion;
    private CharacterController controller;
    
    // Use this for initialization
	void Start () {
        controller = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {
        moveVector = Vector3.zero;
        inputDirection = Input.GetAxis("Horizontal") * speed;

        if (controller.isGrounded)
        {
            verticalVelocity = 0;

            if (Input.GetKeyDown(KeyCode.Space))
            {
                //Make player jump
                verticalVelocity = jumpForce;
                secondJumpAvail = true;
            }

            moveVector.x = inputDirection;
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (secondJumpAvail)
                {
                    verticalVelocity = jumpForce;
                    secondJumpAvail = false;
                }
            }

            verticalVelocity -= gravity * Time.deltaTime;
            moveVector.x = lastMotion.x;
        }

        moveVector.y = verticalVelocity;

        controller.Move(moveVector * Time.deltaTime);
        lastMotion = moveVector;
	}

    private bool IsControllerGrounded()
    {
        Vector3 leftRayStart;
        Vector3 rightRayStart;

        leftRayStart = controller.bounds.center;
        rightRayStart = controller.bounds.center;

        leftRayStart.x -= controller.bounds.extents.x;
        rightRayStart.x += controller.bounds.extents.x;

        if(Physics.Raycast(leftRayStart,Vector3.down,(controller.height/2) + 0.1f))
            return true;

        if(Physics.Raycast(rightRayStart,Vector3.down,(controller.height/2) + 0.1f))
            return true;

        return false;
    }
        private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (controller.collisionFlags == CollisionFlags.Sides)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                moveVector = hit.normal * speed;
                moveVector.y = jumpForce;
                secondJumpAvail = true;
            }
        }
       
        //Collectables
        switch(hit.gameObject.tag)
        {
        case "Coin":
            Destroy(hit.gameObject);
            break;
        case "JumpPad":
            verticalVelocity = jumpForce * 2;
            break;
        case "Teleport":
           transform.position = hit.transform.GetChild(0).position;
            break;
        default:
            break;
        }
    }
}